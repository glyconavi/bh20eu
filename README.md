# ELIXIR BioHackathon-Europe (BH20EU)


## LIPID MAPS

* Convert from SDF to WURCS

```
$ cat LIPIDMAPS_structures_2020-10-01.sdf | java -jar Mol2WURCS-BH19-v4.8.jar -ID LM_ID -stdin -output BH2020EU
```


## SwissLipids

* Convert from SMILES to SDF

 * run 1
```
$ obabel -ismi lipids-smi-id.smi -osdf --gen2d > lipids-smi-id.smi.sdf
==============================
*** Open Babel Error  in ParseComplex
  SMILES string contains a character 'e' which is invalid
76629 molecules converted
```

 * run 2

```
$ obabel -ismi lipids-smi-id_1112.smi -osdf --gen2d > lipids-smi-id_1112.smi.sdf
```

result:
```
 17 19  1  0  0  0  0
M  END
$$$$
SLM:000501503
 OpenBabel11122018142D

 24 24  0  0  1  0  0  0  0  0999 V2000
    0.5000    0.8660    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
    0.5000    1.8660    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3660    2.3660    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3660    3.3660    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
```



 * run 3

```
$ obabel -ismi lipids-smi-id_1112.smi -osdf --gen2d --append > lipids-smi-id_1112-append.smi.sdf

```

result:

```
17 18  2  0  0  0  0
 17 19  1  0  0  0  0
M  END
$$$$
SLM:000501503 CC[C@H](/C=C/C=C\C/C=C\C=C\C=C\[C@@H]1O[C@H]1CCCC(=O)[O-])O
 OpenBabel11122018362D

 24 24  0  0  1  0  0  0  0  0999 V2000
    0.5000    0.8660    0.0000 C   0  0  1  0  0  0  0  0  0  0  0  0
    0.5000    1.8660    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3660    2.3660    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
    1.3660    3.3660    0.00
```


* Convert from SDF to WURCS

```
cat lipids-smi-id.smi.sdf | java -jar Mol2WURCS-BH19-v4.8.jar -ID ID.. -stdin -output BH2020EU
```




