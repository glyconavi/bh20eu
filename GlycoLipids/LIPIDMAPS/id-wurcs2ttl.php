<?php

$dirpath = dirname(__FILE__);
$testtime = "20201111";

$res_dir = opendir( $dirpath.DIRECTORY_SEPARATOR.$testtime);

date_default_timezone_set('Asia/Tokyo');
$fileHeader = date("Y-m-d_H-i-s");

$wfp = fopen($dirpath.DIRECTORY_SEPARATOR.$fileHeader."_LIPIDMAPS_id-WURUCS.ttl", "a");



fwrite($wfp, "@prefix dcterms: <http://purl.org/dc/terms/> .\n");
fwrite($wfp, "@prefix foaf: <http://xmlns.com/foaf/0.1/> .\n");
fwrite($wfp, "@prefix sio: <http://semanticscience.org/resource/> .\n");
fwrite($wfp, "@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .\n");
fwrite($wfp, "@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n");
fwrite($wfp, "@prefix gco:   <http://purl.jp/bio/12/glyco/glycan#> .\n");
 


fwrite($wfp, ""."\n");


$file_name = "20201111null_result.txt ";

while( $file_name = readdir( $res_dir ) ){

        echo $dirpath.DIRECTORY_SEPARATOR.$testtime.DIRECTORY_SEPARATOR.$file_name;

        $fp = fopen($dirpath.DIRECTORY_SEPARATOR.$testtime.DIRECTORY_SEPARATOR.$file_name,"r");


        while($data = fgets($fp)){

            $iw = explode("\t", $data);
            if (count($iw == 3)){
                $id =  str_replace("\n", "", trim($iw[0]));
//                $key = str_replace("\n", "", trim($iw[1]));
                $wurcs = str_replace("\n", "", trim($iw[2]));
            }
            // write rdf data
            // https://www.lipidmaps.org/data/LMSDRecord.php?LMID=LMGP01020046

            $lipidURI = "<http://glyconavi.org/lipid/".$id.">";
            $glycanURI = "<http://glyconavi.org/glycan/".urlencode($wurcs).">";


            fwrite($wfp, "<http://www.lipidmaps.org/data/LMSDRecord.php?LMID=".$id.">"."\n");
            fwrite($wfp, "\t"."gco:has_lipid"."\n");
            fwrite($wfp, "\t".$lipidURI." ;"."\n");
            fwrite($wfp, "\t"."gco:has_glycan"."\n");
            fwrite($wfp, "\t".$glycanURI. " ;"."\n");
            fwrite($wfp, "\t"."dcterms:identifier\t\"".$id."\" ."."\n");

            fwrite($wfp, ""."\n");

            fwrite($wfp, $glycanURI."\n");
            fwrite($wfp, "\t"."<http://semanticscience.org/resource/SIO_000300> \"".$wurcs."\" ; "."\n");
            fwrite($wfp, "\t"."rdf:type <http://glycoinfo.org/notation/wurcs/2.0>."."\n");

            fwrite($wfp, "\t".""."\n");

    }
}
closedir( $res_dir );
fclose($fp);
fclose($wfp);
?>